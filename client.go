package spotify

import (
	"errors"
	"os"
	"time"

	"bitbucket.org/mstenius/logger"
	"github.com/zmb3/spotify"
	"golang.org/x/oauth2"
)

var (
	redirectURL = os.Getenv("SPOTIFY_REDIRECT_URL")
	scopes      = []string{
		spotify.ScopeUserReadPrivate,
		spotify.ScopePlaylistReadPrivate,
		spotify.ScopePlaylistModifyPublic,
		spotify.ScopePlaylistModifyPrivate,
	}
	auth = spotify.NewAuthenticator(redirectURL, scopes...)
	AuthURL = auth.AuthURL(os.Getenv("SPOTIFY_STATE_TOKEN"))
)

// Credentials for accessing Spotify
type Credentials struct {
	AccessToken  string
	RefreshToken string
}

// Client for Spotify
type Client struct {
	spotify.Client
}

// NewByRequestToken get Client based on token in request
func NewByRequest(state, code string) (*Client, error) {
	if state != os.Getenv("SPOTIFY_STATE_TOKEN") {
		logger.WithFields(logger.Fields{"state": state}).Error("Client::NewByRequest state mismatch")
		return nil, errors.New("state mismatch")
	}

	token, err := auth.Exchange(code)
	if err != nil {
		logger.WithFields(logger.Fields{"error": err}).Error("Client::NewByRequest() could not extract token")
		return nil, errors.New("token could not be found")
	}
	return &Client{auth.NewClient(token)}, nil
}

// NewByCredentials get Client based on credentials
func NewByCredentails(credentials Credentials) (*Client, error) {
	token := &oauth2.Token{
		Expiry:       time.Unix(60, 0),
		AccessToken:  credentials.AccessToken,
		RefreshToken: credentials.RefreshToken,
	}
	return &Client{auth.NewClient(token)}, nil
}
